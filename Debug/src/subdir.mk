################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/bldc.c \
../src/main.c \
../src/setup.c \
../src/stm32f1xx_it.c \
../src/syscalls.c \
../src/system_stm32f1xx.c 

OBJS += \
./src/bldc.o \
./src/main.o \
./src/setup.o \
./src/stm32f1xx_it.o \
./src/syscalls.o \
./src/system_stm32f1xx.o 

C_DEPS += \
./src/bldc.d \
./src/main.d \
./src/setup.d \
./src/stm32f1xx_it.d \
./src/syscalls.d \
./src/system_stm32f1xx.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -mfloat-abi=soft -DSTM32 -DSTM32F1 -DSTM32F103RCTx -DDEBUG -DSTM32F103xE -DUSE_HAL_DRIVER -I"C:/Users/123iv/workspace/brushers_rs485/Drivers/STM32F1xx_HAL_Driver/Inc/Legacy" -I"C:/Users/123iv/workspace/brushers_rs485/Drivers/CMSIS/Device/ST/STM32F1xx/Include" -I"C:/Users/123iv/workspace/brushers_rs485/Drivers/CMSIS/Include" -I"C:/Users/123iv/workspace/brushers_rs485/Middlewares/FreeModbus/app" -I"C:/Users/123iv/workspace/brushers_rs485/Middlewares/FreeModbus/ascii" -I"C:/Users/123iv/workspace/brushers_rs485/Middlewares/FreeModbus/functions" -I"C:/Users/123iv/workspace/brushers_rs485/Middlewares/FreeModbus/include" -I"C:/Users/123iv/workspace/brushers_rs485/Middlewares/FreeModbus/port" -I"C:/Users/123iv/workspace/brushers_rs485/Middlewares/FreeModbus/rtu" -I"C:/Users/123iv/workspace/brushers_rs485/Middlewares/FreeModbus/tcp" -I"C:/Users/123iv/workspace/brushers_rs485/inc" -I"C:/Users/123iv/workspace/brushers_rs485/Drivers/STM32F1xx_HAL_Driver/Inc"  -O0 -g3 -Wall -fmessage-length=0 -ffunction-sections -c -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


