################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Middlewares/FreeModbus/mb.c \
../Middlewares/FreeModbus/mb_m.c 

OBJS += \
./Middlewares/FreeModbus/mb.o \
./Middlewares/FreeModbus/mb_m.o 

C_DEPS += \
./Middlewares/FreeModbus/mb.d \
./Middlewares/FreeModbus/mb_m.d 


# Each subdirectory must supply rules for building sources it contributes
Middlewares/FreeModbus/%.o: ../Middlewares/FreeModbus/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -mfloat-abi=soft -DSTM32 -DSTM32F1 -DSTM32F103RCTx -DDEBUG -DSTM32F103xE -DUSE_HAL_DRIVER -I"C:/Users/123iv/workspace/brushers_rs485/Drivers/STM32F1xx_HAL_Driver/Inc/Legacy" -I"C:/Users/123iv/workspace/brushers_rs485/Drivers/CMSIS/Device/ST/STM32F1xx/Include" -I"C:/Users/123iv/workspace/brushers_rs485/Drivers/CMSIS/Include" -I"C:/Users/123iv/workspace/brushers_rs485/Middlewares/FreeModbus/app" -I"C:/Users/123iv/workspace/brushers_rs485/Middlewares/FreeModbus/ascii" -I"C:/Users/123iv/workspace/brushers_rs485/Middlewares/FreeModbus/functions" -I"C:/Users/123iv/workspace/brushers_rs485/Middlewares/FreeModbus/include" -I"C:/Users/123iv/workspace/brushers_rs485/Middlewares/FreeModbus/port" -I"C:/Users/123iv/workspace/brushers_rs485/Middlewares/FreeModbus/rtu" -I"C:/Users/123iv/workspace/brushers_rs485/Middlewares/FreeModbus/tcp" -I"C:/Users/123iv/workspace/brushers_rs485/inc" -I"C:/Users/123iv/workspace/brushers_rs485/Drivers/STM32F1xx_HAL_Driver/Inc"  -O0 -g3 -Wall -fmessage-length=0 -ffunction-sections -c -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


