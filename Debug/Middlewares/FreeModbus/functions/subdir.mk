################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Middlewares/FreeModbus/functions/mbfunccoils.c \
../Middlewares/FreeModbus/functions/mbfunccoils_m.c \
../Middlewares/FreeModbus/functions/mbfuncdiag.c \
../Middlewares/FreeModbus/functions/mbfuncdisc.c \
../Middlewares/FreeModbus/functions/mbfuncdisc_m.c \
../Middlewares/FreeModbus/functions/mbfuncholding.c \
../Middlewares/FreeModbus/functions/mbfuncholding_m.c \
../Middlewares/FreeModbus/functions/mbfuncinput.c \
../Middlewares/FreeModbus/functions/mbfuncinput_m.c \
../Middlewares/FreeModbus/functions/mbfuncother.c \
../Middlewares/FreeModbus/functions/mbutils.c 

OBJS += \
./Middlewares/FreeModbus/functions/mbfunccoils.o \
./Middlewares/FreeModbus/functions/mbfunccoils_m.o \
./Middlewares/FreeModbus/functions/mbfuncdiag.o \
./Middlewares/FreeModbus/functions/mbfuncdisc.o \
./Middlewares/FreeModbus/functions/mbfuncdisc_m.o \
./Middlewares/FreeModbus/functions/mbfuncholding.o \
./Middlewares/FreeModbus/functions/mbfuncholding_m.o \
./Middlewares/FreeModbus/functions/mbfuncinput.o \
./Middlewares/FreeModbus/functions/mbfuncinput_m.o \
./Middlewares/FreeModbus/functions/mbfuncother.o \
./Middlewares/FreeModbus/functions/mbutils.o 

C_DEPS += \
./Middlewares/FreeModbus/functions/mbfunccoils.d \
./Middlewares/FreeModbus/functions/mbfunccoils_m.d \
./Middlewares/FreeModbus/functions/mbfuncdiag.d \
./Middlewares/FreeModbus/functions/mbfuncdisc.d \
./Middlewares/FreeModbus/functions/mbfuncdisc_m.d \
./Middlewares/FreeModbus/functions/mbfuncholding.d \
./Middlewares/FreeModbus/functions/mbfuncholding_m.d \
./Middlewares/FreeModbus/functions/mbfuncinput.d \
./Middlewares/FreeModbus/functions/mbfuncinput_m.d \
./Middlewares/FreeModbus/functions/mbfuncother.d \
./Middlewares/FreeModbus/functions/mbutils.d 


# Each subdirectory must supply rules for building sources it contributes
Middlewares/FreeModbus/functions/%.o: ../Middlewares/FreeModbus/functions/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -mfloat-abi=soft -DSTM32 -DSTM32F1 -DSTM32F103RCTx -DDEBUG -DSTM32F103xE -DUSE_HAL_DRIVER -I"C:/Users/123iv/workspace/brushers_rs485/Drivers/STM32F1xx_HAL_Driver/Inc/Legacy" -I"C:/Users/123iv/workspace/brushers_rs485/Drivers/CMSIS/Device/ST/STM32F1xx/Include" -I"C:/Users/123iv/workspace/brushers_rs485/Drivers/CMSIS/Include" -I"C:/Users/123iv/workspace/brushers_rs485/Middlewares/FreeModbus/app" -I"C:/Users/123iv/workspace/brushers_rs485/Middlewares/FreeModbus/ascii" -I"C:/Users/123iv/workspace/brushers_rs485/Middlewares/FreeModbus/functions" -I"C:/Users/123iv/workspace/brushers_rs485/Middlewares/FreeModbus/include" -I"C:/Users/123iv/workspace/brushers_rs485/Middlewares/FreeModbus/port" -I"C:/Users/123iv/workspace/brushers_rs485/Middlewares/FreeModbus/rtu" -I"C:/Users/123iv/workspace/brushers_rs485/Middlewares/FreeModbus/tcp" -I"C:/Users/123iv/workspace/brushers_rs485/inc" -I"C:/Users/123iv/workspace/brushers_rs485/Drivers/STM32F1xx_HAL_Driver/Inc"  -O0 -g3 -Wall -fmessage-length=0 -ffunction-sections -c -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


