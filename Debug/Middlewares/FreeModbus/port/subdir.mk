################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Middlewares/FreeModbus/port/portcritical.c \
../Middlewares/FreeModbus/port/portevent.c \
../Middlewares/FreeModbus/port/portevent_m.c \
../Middlewares/FreeModbus/port/portserial.c \
../Middlewares/FreeModbus/port/portserial_m.c \
../Middlewares/FreeModbus/port/porttimer.c \
../Middlewares/FreeModbus/port/porttimer_m.c 

OBJS += \
./Middlewares/FreeModbus/port/portcritical.o \
./Middlewares/FreeModbus/port/portevent.o \
./Middlewares/FreeModbus/port/portevent_m.o \
./Middlewares/FreeModbus/port/portserial.o \
./Middlewares/FreeModbus/port/portserial_m.o \
./Middlewares/FreeModbus/port/porttimer.o \
./Middlewares/FreeModbus/port/porttimer_m.o 

C_DEPS += \
./Middlewares/FreeModbus/port/portcritical.d \
./Middlewares/FreeModbus/port/portevent.d \
./Middlewares/FreeModbus/port/portevent_m.d \
./Middlewares/FreeModbus/port/portserial.d \
./Middlewares/FreeModbus/port/portserial_m.d \
./Middlewares/FreeModbus/port/porttimer.d \
./Middlewares/FreeModbus/port/porttimer_m.d 


# Each subdirectory must supply rules for building sources it contributes
Middlewares/FreeModbus/port/%.o: ../Middlewares/FreeModbus/port/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -mfloat-abi=soft -DSTM32 -DSTM32F1 -DSTM32F103RCTx -DDEBUG -DSTM32F103xE -DUSE_HAL_DRIVER -I"C:/Users/123iv/workspace/brushers_rs485/Drivers/STM32F1xx_HAL_Driver/Inc/Legacy" -I"C:/Users/123iv/workspace/brushers_rs485/Drivers/CMSIS/Device/ST/STM32F1xx/Include" -I"C:/Users/123iv/workspace/brushers_rs485/Drivers/CMSIS/Include" -I"C:/Users/123iv/workspace/brushers_rs485/Middlewares/FreeModbus/app" -I"C:/Users/123iv/workspace/brushers_rs485/Middlewares/FreeModbus/ascii" -I"C:/Users/123iv/workspace/brushers_rs485/Middlewares/FreeModbus/functions" -I"C:/Users/123iv/workspace/brushers_rs485/Middlewares/FreeModbus/include" -I"C:/Users/123iv/workspace/brushers_rs485/Middlewares/FreeModbus/port" -I"C:/Users/123iv/workspace/brushers_rs485/Middlewares/FreeModbus/rtu" -I"C:/Users/123iv/workspace/brushers_rs485/Middlewares/FreeModbus/tcp" -I"C:/Users/123iv/workspace/brushers_rs485/inc" -I"C:/Users/123iv/workspace/brushers_rs485/Drivers/STM32F1xx_HAL_Driver/Inc"  -O0 -g3 -Wall -fmessage-length=0 -ffunction-sections -c -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


