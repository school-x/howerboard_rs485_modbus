
#include "stm32f1xx_hal.h"
#include "defines.h"
#include "setup.h"
#include "config.h"


volatile int posl = 0;
volatile int posr = 0;
volatile int pwml = 0;
volatile int pwmr = 0;
volatile int weakl = 0;
volatile int weakr = 0;

volatile int posl_last = 0;
volatile int posr_last = 0;

volatile int posl_now = 0;
volatile int posr_now = 0;

volatile int posl_summ = 0;
volatile int posr_summ = 0;

volatile int d_posr=0;
volatile int d_posl=0;


extern int chatter_last_speed ;

extern int chatter_interval_pid  ;
extern int chatter_last_pid ;

extern int posl_summ_now_w;
extern int posr_summ_now_w;

extern int d_posl_summ_w;
extern int d_posr_summ_w;

extern int posl_summ_last_w;
extern int posr_summ_last_w;

int flag_r = 0;
int flag_l = 0;


volatile float w_l = 0;
volatile float w_r = 0;


extern float v_real ;
extern float w_real ;

//���������
extern float x ;
extern float y ;
extern float theta ;

//���������� ��� ������ � ����������
extern float x_now;
extern float y_now;
extern float theta_now;

extern float x_last;
extern float y_last;
extern float theta_last;

extern float d_x;
extern float d_y;
extern float d_theta_s;

extern float d_s_m;

//���������� ��� ������� ���������� ���������
extern float x_now_s;
extern float y_now_s;

extern float x_last_s;
extern float y_last_s;

extern float d_x_s;
extern float d_y_s;


extern float R;
extern float x_r;
extern float y_r;

//float pwm_start = 40.00;
float pwm_start = 0.0;
float pid_pwm_start_l = 0.00;
float pid_pwm_start_r = 0.00;

extern float Kp_w_l ;
extern float Ki_w_l ;
extern float Kd_w_l ;

extern float error_w_l ;
extern float total_error_w_l ;
extern float d_error_w_l ;
extern float p_error_w_l ;
extern float last_error_w_l;

float last_last_error_w_l =0.0;

extern float pid_w_l ;

extern float last_pid_w_l;

extern float Kp_w_r ;
extern float Ki_w_r ;
extern float Kd_w_r ;

extern float error_w_r ;
extern float total_error_w_r ;
extern float d_error_w_r ;
extern float p_error_w_r ;
extern float last_error_w_r ;

float last_last_error_w_r;

extern float pid_w_r ;

extern float last_pid_w_r;

extern float w_to_l ;
extern float w_to_r ;

extern int lastSpeedL , lastSpeedR ;
extern int speedL , speedR ;
extern int speedL2 , speedR2;
extern float direction ;

extern float L_sepr ;
extern float R_wheel ;
extern float pi;

extern unsigned long time_cmd_vel_d;
extern unsigned long time_cmd_vel_now; //
extern unsigned long time_cmd_vel_last; //


extern volatile int speed;

extern volatile adc_buf_t adc_buffer;

extern volatile uint32_t timeout;

//extern volatile int Millis_R;
//extern volatile int Millis_L;



uint32_t buzzerFreq = 0;
uint32_t buzzerPattern = 0;

uint8_t enable = 0;

const int pwm_res = 64000000 / 2 / PWM_FREQ; // = 2000

const uint8_t hall_to_pos[8] = {
    0,
    0,
    2,
    1,
    4,
    5,
    3,
    0,
};

static inline void blockPWM(int pwm, int pos, int *u, int *v, int *w) {
  switch(pos) {
    case 0:
      *u = 0;
      *v = pwm;
      *w = -pwm;
      break;
    case 1:
      *u = -pwm;
      *v = pwm;
      *w = 0;
      break;
    case 2:
      *u = -pwm;
      *v = 0;
      *w = pwm;
      break;
    case 3:
      *u = 0;
      *v = -pwm;
      *w = pwm;
      break;
    case 4:
      *u = pwm;
      *v = -pwm;
      *w = 0;
      break;
    case 5:
      *u = pwm;
      *v = 0;
      *w = -pwm;
      break;
    default:
      *u = 0;
      *v = 0;
      *w = 0;
  }
}

static inline void blockPhaseCurrent(int pos, int u, int v, int *q) {
  switch(pos) {
    case 0:
      *q = u - v;
      // *u = 0;
      // *v = pwm;
      // *w = -pwm;
      break;
    case 1:
      *q = u;
      // *u = -pwm;
      // *v = pwm;
      // *w = 0;
      break;
    case 2:
      *q = u;
      // *u = -pwm;
      // *v = 0;
      // *w = pwm;
      break;
    case 3:
      *q = v;
      // *u = 0;
      // *v = -pwm;
      // *w = pwm;
      break;
    case 4:
      *q = v;
      // *u = pwm;
      // *v = -pwm;
      // *w = 0;
      break;
    case 5:
      *q = -(u - v);
      // *u = pwm;
      // *v = 0;
      // *w = -pwm;
      break;
    default:
      *q = 0;
      // *u = 0;
      // *v = 0;
      // *w = 0;
  }
}

uint32_t buzzerTimer        = 0;

int offsetcount = 0;
int offsetrl1   = 2000;
int offsetrl2   = 2000;
int offsetrr1   = 2000;
int offsetrr2   = 2000;
int offsetdcl   = 2000;
int offsetdcr   = 2000;

float batteryVoltage = BAT_NUMBER_OF_CELLS * 4.0;

int curl = 0;
// int errorl = 0;
// int kp = 5;
// volatile int cmdl = 0;

int last_pos = 0;
int timer = 0;
const int max_time = PWM_FREQ / 10;
volatile int vel = 0;

//scan 8 channels with 2ADCs @ 20 clk cycles per sample
//meaning ~80 ADC clock cycles @ 8MHz until new DMA interrupt =~ 100KHz
//=640 cpu cycles
void DMA1_Channel1_IRQHandler() {


  	int d_t =70;
	if(HAL_GetTick() - chatter_last_speed > d_t)
	{
		chatter_last_speed = HAL_GetTick();

		posl_summ_now_w=posl_summ;
		posr_summ_now_w=posr_summ;
		//�������������� �����
		d_posl_summ_w=posl_summ_now_w-posl_summ_last_w;
		d_posl_summ_w=-1*d_posl_summ_w;
		d_posr_summ_w=posr_summ_now_w-posr_summ_last_w;
		//		d_posr_summ_w=-1*d_posr_summ_w;

		posl_summ_last_w=posl_summ_now_w;
		posr_summ_last_w=posr_summ_now_w;


		d_x=x_now-x_last;
		d_y=y_now-y_last;
		d_theta_s=theta_now-theta_last;

		float d_ttt=1000/d_t; // ������� �������, �� ����� �������� ��� ��� ���������� ����� ��� �������

		//������� �������� ������� ������
		w_l=d_posl_summ_w*d_ttt*2*pi/90;
		w_r=d_posr_summ_w*d_ttt*2*pi/90;


		//���������� ���������
		v_real=(w_l*R_wheel+w_r*R_wheel)/2.0;
		w_real=(w_l*R_wheel - w_r*R_wheel)/L_sepr;



		chatter_last_pid = HAL_GetTick();

		error_w_l=w_to_l-(w_l);
		p_error_w_l=error_w_l-last_error_w_l;
		d_error_w_l=error_w_l-2*last_error_w_l + last_last_error_w_l;

		last_last_error_w_l = last_error_w_l;
		last_error_w_l = error_w_l;
//		����������� �� ����������� ����� ����� ����� �� �� ���� ��� ����� �������� ��� �� �� ����� ������������ � �� ����� �������� ���
//		if (w_l==0  && abs(pid_w_l)< 50 ){
//			last_pid_w_l = 0;
//		}
////		����������� �� ����� ��� ����������� ���������� �������


		if (w_to_l>0 && w_l > 0){
			pid_pwm_start_l = pwm_start;
			pid_w_l= pid_pwm_start_l + last_pid_w_l + Kp_w_l*p_error_w_l + Ki_w_l*error_w_l  + Kd_w_l*d_error_w_l;
			last_pid_w_l = last_pid_w_l + Kp_w_l*p_error_w_l + Ki_w_l*error_w_l  + Kd_w_l*d_error_w_l;
		}
		else if (w_to_l>0 && w_l == 0){
			pid_pwm_start_l = pwm_start;
			pid_w_l= pid_pwm_start_l + last_pid_w_l + Kp_w_l*p_error_w_l + Ki_w_l*error_w_l  + Kd_w_l*d_error_w_l;
			last_pid_w_l = last_pid_w_l + Kp_w_l*p_error_w_l + Ki_w_l*error_w_l  + Kd_w_l*d_error_w_l;
		}
		else if (w_to_l>0 && w_l < 0 ){
			pid_pwm_start_l = pwm_start;
			pid_w_l= pid_pwm_start_l + last_pid_w_l + Kp_w_l*p_error_w_l + Ki_w_l*error_w_l  + Kd_w_l*d_error_w_l;
			last_pid_w_l = last_pid_w_l + Kp_w_l*p_error_w_l + Ki_w_l*error_w_l  + Kd_w_l*d_error_w_l;
		}

		if (w_to_l == 0 && w_l > 0){
			pid_pwm_start_l = -pwm_start*0.3;
			pid_w_l= pid_pwm_start_l + last_pid_w_l + Kp_w_l*p_error_w_l + Ki_w_l*error_w_l  + Kd_w_l*d_error_w_l;
			last_pid_w_l = pid_w_l;

		}
		else if (w_to_l == 0 && w_l == 0){
			pid_pwm_start_l = 0;
		}
		else if (w_to_l == 0 && w_l < 0 ){
			pid_pwm_start_l = pwm_start*0.3;
			pid_w_l= pid_pwm_start_l + last_pid_w_l + Kp_w_l*p_error_w_l + Ki_w_l*error_w_l  + Kd_w_l*d_error_w_l;
			last_pid_w_l = pid_w_l;

		}

		if (w_to_l < 0 && w_l > 0){
			pid_pwm_start_l = -pwm_start;
			pid_w_l= pid_pwm_start_l + last_pid_w_l + Kp_w_l*p_error_w_l + Ki_w_l*error_w_l  + Kd_w_l*d_error_w_l;
			last_pid_w_l = last_pid_w_l + Kp_w_l*p_error_w_l + Ki_w_l*error_w_l  + Kd_w_l*d_error_w_l;
		}
		else if (w_to_l < 0 && w_l == 0){
			pid_pwm_start_l = -pwm_start;
			pid_w_l= pid_pwm_start_l + last_pid_w_l + Kp_w_l*p_error_w_l + Ki_w_l*error_w_l  + Kd_w_l*d_error_w_l;
			last_pid_w_l = last_pid_w_l + Kp_w_l*p_error_w_l + Ki_w_l*error_w_l  + Kd_w_l*d_error_w_l;
		}
		else if (w_to_l < 0 && w_l < 0 ){
			pid_pwm_start_l = -pwm_start;
			pid_w_l= pid_pwm_start_l + last_pid_w_l + Kp_w_l*p_error_w_l + Ki_w_l*error_w_l  + Kd_w_l*d_error_w_l;
			last_pid_w_l = last_pid_w_l + Kp_w_l*p_error_w_l + Ki_w_l*error_w_l  + Kd_w_l*d_error_w_l;
		}


//		pid_w_l= pid_pwm_start + last_pid_w_l + Kp_w_l*p_error_w_l + Ki_w_l*error_w_l  + Kd_w_l*d_error_w_l;
//		last_pid_w_l = last_pid_w_l + Kp_w_l*p_error_w_l + Ki_w_l*error_w_l  + Kd_w_l*d_error_w_l;



		error_w_r=w_to_r-(w_r);
		p_error_w_r=error_w_r-last_error_w_r;
		d_error_w_r=error_w_r - 2*last_error_w_r + last_last_error_w_r;

		last_last_error_w_r = last_error_w_r;
		last_error_w_r = error_w_r;

		if (w_to_r>0 && w_r > 0){
			pid_pwm_start_r = pwm_start;
			pid_w_r= pid_pwm_start_r + last_pid_w_r + Kp_w_r*p_error_w_r + Ki_w_r*error_w_r  + Kd_w_r*d_error_w_r;
			last_pid_w_r = last_pid_w_r + Kp_w_r*p_error_w_r + Ki_w_r*error_w_r  + Kd_w_r*d_error_w_r;
		}
		else if (w_to_r>0 && w_r == 0){
			pid_pwm_start_r = pwm_start;
			pid_w_r= pid_pwm_start_r + last_pid_w_r + Kp_w_r*p_error_w_r + Ki_w_r*error_w_r  + Kd_w_r*d_error_w_r;
			last_pid_w_r = last_pid_w_r + Kp_w_r*p_error_w_r + Ki_w_r*error_w_r  + Kd_w_r*d_error_w_r;
		}
		else if (w_to_r>0 && w_r < 0 ){
			pid_pwm_start_r = pwm_start;
			pid_w_r= pid_pwm_start_r + last_pid_w_r + Kp_w_r*p_error_w_r + Ki_w_r*error_w_r  + Kd_w_r*d_error_w_r;
			last_pid_w_r = last_pid_w_r + Kp_w_r*p_error_w_r + Ki_w_r*error_w_r  + Kd_w_r*d_error_w_r;
		}

		if (w_to_r == 0 && w_r > 0){
			pid_pwm_start_r = -pwm_start*0.3;
			pid_w_r= pid_pwm_start_r + last_pid_w_r + Kp_w_r*p_error_w_r + Ki_w_r*error_w_r  + Kd_w_r*d_error_w_r;
			last_pid_w_r = pid_w_r;

		}
		else if (w_to_r == 0 && w_r == 0){
			pid_pwm_start_r = 0;
		}
		else if (w_to_r == 0 && w_r < 0 ){
			pid_pwm_start_r = pwm_start*0.3;
			pid_w_r= pid_pwm_start_r + last_pid_w_r + Kp_w_r*p_error_w_r + Ki_w_r*error_w_r  + Kd_w_r*d_error_w_r;
			last_pid_w_r = pid_w_r;

		}

		if (w_to_r < 0 && w_r > 0){
			pid_pwm_start_r = -pwm_start;
			pid_w_r= pid_pwm_start_r + last_pid_w_r + Kp_w_r*p_error_w_r + Ki_w_r*error_w_r  + Kd_w_r*d_error_w_r;
			last_pid_w_r = last_pid_w_r + Kp_w_r*p_error_w_r + Ki_w_r*error_w_r  + Kd_w_r*d_error_w_r;
		}
		else if (w_to_r < 0 && w_r == 0){
			pid_pwm_start_r = -pwm_start;
			pid_w_r= pid_pwm_start_r + last_pid_w_r + Kp_w_r*p_error_w_r + Ki_w_r*error_w_r  + Kd_w_r*d_error_w_r;
			last_pid_w_r = last_pid_w_r + Kp_w_r*p_error_w_r + Ki_w_r*error_w_r  + Kd_w_r*d_error_w_r;
		}
		else if (w_to_r < 0 && w_r < 0 ){
			pid_pwm_start_r = -pwm_start;
			pid_w_r= pid_pwm_start_r + last_pid_w_r + Kp_w_r*p_error_w_r + Ki_w_r*error_w_r  + Kd_w_r*d_error_w_r;
			last_pid_w_r = last_pid_w_r + Kp_w_r*p_error_w_r + Ki_w_r*error_w_r  + Kd_w_r*d_error_w_r;
		}


//		pid_w_r= last_pid_w_r + Kp_w_r*p_error_w_r + Ki_w_r*error_w_r  + Kd_w_r*d_error_w_r;
//		last_pid_w_r = pid_w_r;

		if(w_to_r == 0){
			pid_w_r = 0;
		}
		if(w_to_l == 0){
			pid_w_l = 0;
		}

		int max_pid = 900;

		if (pid_w_l>=max_pid){
			pid_w_l=max_pid;
		}
		if (pid_w_l<=-max_pid){
			pid_w_l=-max_pid;
		}

		if (pid_w_r>=max_pid){
			pid_w_r=max_pid;
		}
		if (pid_w_r<=-max_pid){
			pid_w_r=-max_pid;
		}




		pwml=pid_w_l;
		pwmr=-pid_w_r;

//		pwml=0;
//		pwmr=0;


	}





  DMA1->IFCR = DMA_IFCR_CTCIF1;
  // HAL_GPIO_WritePin(LED_PORT, LED_PIN, 1);

  if(offsetcount < 1000) {  // calibrate ADC offsets
    offsetcount++;
    offsetrl1 = (adc_buffer.rl1 + offsetrl1) / 2;
    offsetrl2 = (adc_buffer.rl2 + offsetrl2) / 2;
    offsetrr1 = (adc_buffer.rr1 + offsetrr1) / 2;
    offsetrr2 = (adc_buffer.rr2 + offsetrr2) / 2;
    offsetdcl = (adc_buffer.dcl + offsetdcl) / 2;
    offsetdcr = (adc_buffer.dcr + offsetdcr) / 2;
    return;
  }

  if (buzzerTimer % 1000 == 0) {  // because you get float rounding errors if it would run every time
    batteryVoltage = batteryVoltage * 0.99 + ((float)adc_buffer.batt1 * ((float)BAT_CALIB_REAL_VOLTAGE / (float)BAT_CALIB_ADC)) * 0.01;
//    batteryVoltage = ((float)adc_buffer.batt1 * 16 -1024)/639;

  }

  //disable PWM when current limit is reached (current chopping)
  if(ABS((adc_buffer.dcl - offsetdcl) * MOTOR_AMP_CONV_DC_AMP) > DC_CUR_LIMIT || timeout > TIMEOUT || enable == 0) {
    LEFT_TIM->BDTR &= ~TIM_BDTR_MOE;
    //HAL_GPIO_WritePin(LED_PORT, LED_PIN, 1);
  } else {
    LEFT_TIM->BDTR |= TIM_BDTR_MOE;
    //HAL_GPIO_WritePin(LED_PORT, LED_PIN, 0);
  }

  if(ABS((adc_buffer.dcr - offsetdcr) * MOTOR_AMP_CONV_DC_AMP)  > DC_CUR_LIMIT || timeout > TIMEOUT || enable == 0) {
    RIGHT_TIM->BDTR &= ~TIM_BDTR_MOE;
  } else {
    RIGHT_TIM->BDTR |= TIM_BDTR_MOE;
  }

  int ul, vl, wl;
  int ur, vr, wr;

  //determine next position based on hall sensors
  uint8_t hall_ul = !(LEFT_HALL_U_PORT->IDR & LEFT_HALL_U_PIN);
  uint8_t hall_vl = !(LEFT_HALL_V_PORT->IDR & LEFT_HALL_V_PIN);
  uint8_t hall_wl = !(LEFT_HALL_W_PORT->IDR & LEFT_HALL_W_PIN);

  uint8_t hall_ur = !(RIGHT_HALL_U_PORT->IDR & RIGHT_HALL_U_PIN);
  uint8_t hall_vr = !(RIGHT_HALL_V_PORT->IDR & RIGHT_HALL_V_PIN);
  uint8_t hall_wr = !(RIGHT_HALL_W_PORT->IDR & RIGHT_HALL_W_PIN);

  uint8_t halll = hall_ul * 1 + hall_vl * 2 + hall_wl * 4;
  posl          = hall_to_pos[halll];

  posl_now=posl ;
  if (flag_l==0){
	  posl_last=posl_now;
	  flag_l=1;
  }
  if(posl_now==0 && posl_last==5){
    d_posl=1;
  }
  else if(posl_now==5 && posl_last==0){
    d_posl=-1;
  }
  else{
    d_posl= posl_now-posl_last;
  }
  if (d_posl ==1 | posl ==-1 ){
//	  float dt_1d_l = 160/Millis_L;
//	  w_l=100*(d_posl*dt_1d_l)*2*pi/90;
//	  Millis_L=0;
  }
   posl_summ += d_posl ;
  posl_last=posl ;

  posl += 2;
  posl %= 6;





  uint8_t hallr = hall_ur * 1 + hall_vr * 2 + hall_wr * 4;
  posr          = hall_to_pos[hallr];


  posr_now=posr ;
  if (flag_r==0){
	  posr_last=posr_now;
	  flag_r=1;
  }
  if(posr_now==0 && posr_last==5){
    d_posr=1;
  }
  else if(posr_now==5 && posr_last==0){
    d_posr=-1;
  }
  else{
    d_posr= posr_now-posr_last;
  }

  if (d_posr >=1 || d_posr <=-1 ){
//	  float dt_1d_r = 160/Millis_R;
//	  w_r=100*(d_posr*dt_1d_r)*2*pi/90;
//	  Millis_R_now=Millis_R;
//	  d_Millis_R=Millis_R_now-Millis_R_last;
//	  Millis_R_last=Millis_R;
  }

   posr_summ += d_posr ;
  posr_last=posr ;


  posr += 2;
  posr %= 6;




  blockPhaseCurrent(posl, adc_buffer.rl1 - offsetrl1, adc_buffer.rl2 - offsetrl2, &curl);

  //setScopeChannel(2, (adc_buffer.rl1 - offsetrl1) / 8);
  //setScopeChannel(3, (adc_buffer.rl2 - offsetrl2) / 8);


  // uint8_t buzz(uint16_t *notes, uint32_t len){
    // static uint32_t counter = 0;
    // static uint32_t timer = 0;
    // if(len == 0){
        // return(0);
    // }

    // struct {
        // uint16_t freq : 4;
        // uint16_t volume : 4;
        // uint16_t time : 8;
    // } note = notes[counter];

    // if(timer / 500 == note.time){
        // timer = 0;
        // counter++;
    // }

    // if(counter == len){
        // counter = 0;
    // }

    // timer++;
    // return(note.freq);
  // }


  //create square wave for buzzer
  buzzerTimer++;
  if (buzzerFreq != 0 && (buzzerTimer / 5000) % (buzzerPattern + 1) == 0) {
    if (buzzerTimer % buzzerFreq == 0) {
//      HAL_GPIO_TogglePin(BUZZER_PORT, BUZZER_PIN);
    }
  } else {
//      HAL_GPIO_WritePin(BUZZER_PORT, BUZZER_PIN, 0);
  }

  //update PWM channels based on position
  blockPWM(pwml, posl, &ul, &vl, &wl);
  blockPWM(pwmr, posr, &ur, &vr, &wr);

  int weakul, weakvl, weakwl;
  if (pwml > 0) {
    blockPWM(weakl, (posl+5) % 6, &weakul, &weakvl, &weakwl);
  } else {
    blockPWM(-weakl, (posl+1) % 6, &weakul, &weakvl, &weakwl);
  }
  ul += weakul;
  vl += weakvl;
  wl += weakwl;

  int weakur, weakvr, weakwr;
  if (pwmr > 0) {
    blockPWM(weakr, (posr+5) % 6, &weakur, &weakvr, &weakwr);
  } else {
    blockPWM(-weakr, (posr+1) % 6, &weakur, &weakvr, &weakwr);
  }
  ur += weakur;
  vr += weakvr;
  wr += weakwr;

  LEFT_TIM->LEFT_TIM_U = CLAMP(ul + pwm_res / 2, 10, pwm_res-10);
  LEFT_TIM->LEFT_TIM_V = CLAMP(vl + pwm_res / 2, 10, pwm_res-10);
  LEFT_TIM->LEFT_TIM_W = CLAMP(wl + pwm_res / 2, 10, pwm_res-10);

  RIGHT_TIM->RIGHT_TIM_U = CLAMP(ur + pwm_res / 2, 10, pwm_res-10);
  RIGHT_TIM->RIGHT_TIM_V = CLAMP(vr + pwm_res / 2, 10, pwm_res-10);
  RIGHT_TIM->RIGHT_TIM_W = CLAMP(wr + pwm_res / 2, 10, pwm_res-10);
}
