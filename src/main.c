/*
* This file is part of the hoverboard-firmware-hack project.
*
* Copyright (C) 2017-2018 Rene Hopf <renehopf@mac.com>
* Copyright (C) 2017-2018 Nico Stute <crinq@crinq.de>
* Copyright (C) 2017-2018 Niklas Fauth <niklas.fauth@kit.fail>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "stm32f1xx_hal.h"
#include "defines.h"
#include "setup.h"
#include "config.h"
#include "math.h"
#include "float.h"
#include "main.h"
//#include "hd44780.h"

#include "mb.h"
#include "mbport.h"
#include "user_mb_app.h"


#define WHEEL_NUM                        2

#define LEFT                             0
#define RIGHT                            1

static void SystemClock_Config(void);



//static void MX_GPIO_Init(void);
static void MX_USART2_UART_Init(void);

int ros_speed_r=0; //test
int ros_speed_l=0; //test

float speed_x = 0.0; //РїРѕСЃС‚СѓРїР°С‚РµР»СЊРЅР°СЏ СЃРєРѕСЂРѕСЃС‚СЊ
float speed_w = 0.0; //РІСЂР°С‰Р°С‚РµР»СЊРЅР°СЏ СЃРєРѕСЂРѕСЃС‚СЊ

volatile float v_real = 0.0;
volatile float w_real = 0.0;

float d_r_m=0.000; //
float d_l_m=0.000;
float d_theta=0.000;

// СѓРіР»РѕРІС‹Рµ СЃРєРѕСЂРѕСЃС‚Рё РєРѕР»РµСЃ
extern float w_l;
extern float w_r;


//РѕРґРѕРјРµС‚СЂРёСЏ
volatile float x = 0.0;
volatile float y = 0.0;
volatile float theta = 0.0;

//РїРµСЂРµРјРµРЅРЅС‹Рµ РґР»СЏ СЂР°Р±РѕС‚С‹ СЃ РѕРґРѕРјРµС‚СЂРёРµР№
volatile float x_now=0.000;
volatile float y_now=0.000;
volatile float theta_now=0.000;

volatile float x_last=0.000;
volatile float y_last=0.000;
volatile float theta_last=0.000;

volatile float d_x=0.000;
volatile float d_y=0.000;
volatile float d_theta_s=0.000;

volatile float d_s_m=0.000;

//РїРµСЂРµРјРµРЅРЅС‹Рµ РґР»СЏ СЂР°СЃС‡РµС‚Р° РїСЂРѕР№РґРµРЅРѕРіРѕ СЂР°СЃС‚РѕСЏРЅРёСЏ
volatile float x_now_s=0.000;
volatile float y_now_s=0.000;

volatile float x_last_s=0.000;
volatile float y_last_s=0.000;

volatile float d_x_s=0.000;
volatile float d_y_s=0.000;


volatile float R=0.000;
volatile float x_r=0.000;
volatile float y_r=0.000;



volatile float Kp_w_l = 0.2;
volatile float Ki_w_l = 1.1;
volatile float Kd_w_l = 0.0;

volatile float error_w_l = 0.0;
volatile float total_error_w_l = 0.0;
volatile float p_error_w_l = 0.0;
volatile float d_error_w_l = 0.0;
volatile float last_error_w_l=0;

volatile float last_pid_w_l =0.0;

volatile float pid_w_l = 0.0;

volatile float Kp_w_r = 0.2;
volatile float Ki_w_r = 1.1;
volatile float Kd_w_r = 0.0;

volatile float error_w_r = 0.0;
volatile float total_error_w_r = 0.0;
volatile float p_error_w_r = 0.0 ;
volatile float d_error_w_r = 0.0;
volatile float last_error_w_r = 0.0;

volatile float last_pid_w_r =0.0;


volatile float pid_w_r = 0.0;

volatile float w_to_l = 0.0; //СѓРіР»РѕРІР°СЏ СЃРєРѕСЂРѕСЃС‚СЊ Р»РµРІРѕРіРѕ РєРѕР»РµСЃР° (РєРѕС‚РѕСЂРѕРµ РґРѕР»Р¶РЅРѕ Р±С‹С‚СЊ)
volatile float w_to_r = 0.0; //СѓРіР»РѕРІР°СЏ СЃРєРѕСЂРѕСЃС‚СЊ РїСЂР°РІРѕРіРѕ РєРѕР»РµСЃР° (РєРѕС‚РѕСЂРѕРµ РґРѕР»Р¶РЅРѕ Р±С‹С‚СЊ)



//РєРѕРЅСЃС‚Р°РЅС‚С‹
volatile float L_sepr = 0.465;
volatile float R_wheel = 0.0825;
volatile float pi=3.14159265359;

volatile unsigned long time_cmd_vel_d = 0;
volatile unsigned long time_cmd_vel_now = 0;
volatile unsigned long time_cmd_vel_last = 0;


int cmd_stop_val=1;
int cmd_start_val=0;

//СЃСѓРјР° С‚РёРєРѕРІ СЃ РєРѕР»РµСЃ
extern volatile int posl_summ;
extern volatile int posr_summ;

//РїРµСЂРµРјРµРЅРЅС‹Рµ РґР»СЏ СЂР°Р±РѕС‚С‹ СЃ С‚РёРєР°РјРё
volatile int posl_summ_now=0;
volatile int posr_summ_now=0;

volatile int posl_summ_last=0;
volatile int posr_summ_last=0;

volatile int d_posr_summ=0;
volatile int d_posl_summ=0;

volatile int posl_summ_now_w=0;
volatile int posr_summ_now_w=0;

volatile int d_posl_summ_w=0;
volatile int d_posr_summ_w=0;

volatile int posl_summ_last_w=0;
volatile int posr_summ_last_w=0;

//РїРµСЂРµРјРµРЅРЅС‹Рµ РґР»СЏ СЂР°Р±РѕС‚С‹ СЃ РѕР±РѕСЂРѕС‚Р°РјРё СЂРѕР±РѕС‚Р°
float rpm_left = 0;
float rpm_right = 0;


volatile int lastSpeedL = 0, lastSpeedR = 0;
volatile int speedL = 0, speedR = 0;
volatile int speedL2 = 0, speedR2 = 0;
volatile float direction = 1;






volatile int chatter_interval = 0;
volatile int chatter_last = 0;


volatile int chatter_interval_odom  = 0;
volatile int chatter_last_odom = 0;


volatile int chatter_last_speed = 0;

volatile int chatter_interval_pid  = 0;
volatile int chatter_last_pid = 0;

void poweroff(void);






extern TIM_HandleTypeDef htim_left;
extern TIM_HandleTypeDef htim_right;
extern ADC_HandleTypeDef hadc1;
extern ADC_HandleTypeDef hadc2;
extern volatile adc_buf_t adc_buffer;


TIM_HandleTypeDef htim4;
UART_HandleTypeDef huart2;


uint8_t button1, button2;

int steer; // global variable for steering. -1000 to 1000
int speed; // global variable for speed. -1000 to 1000

extern volatile int pwml;  // global variable for pwm left. -1000 to 1000
extern volatile int pwmr;  // global variable for pwm right. -1000 to 1000
extern volatile int weakl; // global variable for field weakening left. -1000 to 1000
extern volatile int weakr; // global variable for field weakening right. -1000 to 1000

extern uint8_t buzzerFreq;    // global variable for the buzzer pitch. can be 1, 2, 3, 4, 5, 6, 7...
extern uint8_t buzzerPattern; // global variable for the buzzer pattern. can be 1, 2, 3, 4, 5, 6, 7...

extern uint8_t enable; // global variable for motor enable

//extern volatile uint32_t timeout; // global variable for timeout
uint32_t timeout = 0;
extern float batteryVoltage; // global variable for battery voltage

uint32_t inactivity_timeout_counter;

extern uint8_t nunchuck_data[6];

int milli_vel_error_sum = 0;



extern uint16_t usSRegInBuf[];
extern uint16_t usSRegHoldBuf[];
extern uint8_t ucSDiscInBuf[];

//float board_temp_adc_filtered = 0 ;
//board_temp_adc_filtered =  float()adc_buffer.temp;
float board_temp_deg_c;

uint8_t buf_from_stm_drive[5];


float regs_to_float(uint16_t reg_1, uint16_t reg_2){
	uint8_t b0 = reg_1 % 256;
	uint8_t b1 = reg_1 >> 8;
	uint8_t b2 = reg_2 % 256;
	uint8_t b3 = reg_2 >> 8;

	float f;
	uint8_t b[] = {b0, b1, b2, b3};
	memcpy(&f, &b, sizeof(f));
	return f;
}

int regs_to_int32(uint16_t reg_1, uint16_t reg_2){
	uint8_t b0 = reg_1 % 256;
	uint8_t b1 = reg_1 >> 8;
	uint8_t b2 = reg_2 % 256;
	uint8_t b3 = reg_2 >> 8;

	int int32;
	uint8_t b[] = {b0, b1, b2, b3};
	memcpy(&int32, &b, sizeof(int32));
	return int32;
}


uint16_t float_regs[2] = {0, 0};
uint8_t byte_array[]= {0, 0, 0, 0};

void float_to_regs(float f_test){
	for(int i=0; i<4; i++){
	  byte_array[i] = *((char *)&f_test + i);
	}

	float_regs[0] = byte_array[1]*256 + byte_array[0];
	float_regs[1] = byte_array[3]*256 + byte_array[2];
}

int int32_test = 0;
uint16_t int32_rags[2] = {0, 0};

void int32_to_regs(int int32){
	for(int i=0; i<4; i++){
	  byte_array[i] = *((char *)&int32 + i);
	}

	int32_rags[0] = byte_array[1]*256 + byte_array[0];
	int32_rags[1] = byte_array[3]*256 + byte_array[2];
}

uint16_t flag_rpm_cb = 0;
//функция обработки топика RPM
void rpm_callback(int rpm_msg){

	if(flag_rpm_cb==1){
		rpm_left = rpm_msg;
		rpm_right = rpm_msg;

		time_cmd_vel_last = HAL_GetTick();


		w_to_l= (rpm_left * 2.0 * pi) / 60.0;
		w_to_r= (rpm_right * 2.0 * pi) / 60.0;

		if(rpm_msg == 0){
			last_pid_w_r = 0.0;
			last_pid_w_l = 0.0;
		}
		flag_rpm_cb = 0;
		usSRegHoldBuf[7]=0;
	}
}



int main(void) {
  HAL_Init();
  __HAL_RCC_AFIO_CLK_ENABLE();
  HAL_NVIC_SetPriorityGrouping(NVIC_PRIORITYGROUP_4);
  /* System interrupt init*/
  /* MemoryManagement_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(MemoryManagement_IRQn, 0, 0);
  /* BusFault_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(BusFault_IRQn, 0, 0);
  /* UsageFault_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(UsageFault_IRQn, 0, 0);
  /* SVCall_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SVCall_IRQn, 0, 0);
  /* DebugMonitor_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DebugMonitor_IRQn, 0, 0);
  /* PendSV_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(PendSV_IRQn, 0, 0);
  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);

  SystemClock_Config();

  __HAL_RCC_DMA1_CLK_DISABLE();
  MX_GPIO_Init();
  MX_TIM_Init();
  MX_ADC1_Init();
  MX_ADC2_Init();
  MX_TIM4_Init();




  HAL_GPIO_WritePin(OFF_PORT, OFF_PIN, GPIO_PIN_SET);

  HAL_ADC_Start(&hadc1);
  HAL_ADC_Start(&hadc2);

  for (int i = 8; i >= 0; i--) {
	HAL_GPIO_TogglePin(LED_PORT, LED_PIN);
//	HAL_GPIO_TogglePin(RS485_RTS_GPIO_Port, RS485_RTS_Pin);
    HAL_Delay(100);
  }

  HAL_GPIO_WritePin(LED_PORT, LED_PIN, GPIO_PIN_SET);

//  int lastSpeedL = 0, lastSpeedR = 0;
//  int speedL = 0, speedR = 0;
//  int speedL2 = 0, speedR2 = 0;
//  float direction = 1;

    MX_USART2_UART_Init();

    enable = 1;  // enable motors

	chatter_interval = 1000.0 / 50; //РїСѓР±Р»РёРєР°С†РёСЏ СЃ С‡Р°СЃС‚РѕС‚РѕР№ 50 РіРµСЂС†
	chatter_last = HAL_GetTick();

	chatter_interval_odom  = 1000.0 / 50;
	chatter_last_odom = HAL_GetTick();

	chatter_last_speed = HAL_GetTick();

	chatter_interval_pid  = 1000.0 / 10;
	chatter_last_pid = HAL_GetTick();


	uint8_t test_connekted[3];

	uint8_t str[]="USART Transmit\r\n";

//	eMBInit( MB_RTU, 0x01, &huart2, 115200, &htim4 );
	eMBInit( MB_RTU, 0x02, &huart2, 57600, &htim4 );
	eMBEnable( );
	uint32_t last_time = HAL_GetTick();


  while(1) {
	  eMBPoll();

	  if(HAL_GetTick() - last_time > 1000){
//		  usSRegInBuf[0] = HAL_GetTick()/1000;
		  usSRegHoldBuf[0] = HAL_GetTick()/1000;
		  last_time = HAL_GetTick();
	  }

	  flag_rpm_cb = usSRegHoldBuf[7];
	  int rpm = regs_to_int32(usSRegHoldBuf[5], usSRegHoldBuf[6]);
	  rpm_callback(rpm);


//	  from
	  int32_to_regs(posr_summ);
	  usSRegHoldBuf[1] = int32_rags[0];
	  usSRegHoldBuf[2] = int32_rags[1];

	  int32_to_regs(posl_summ);
	  usSRegHoldBuf[3] = int32_rags[0];
	  usSRegHoldBuf[4] = int32_rags[1];

      timeout = 0;
      // СЂР°СЃС‡РµС‚ С‚РµРјРїРµСЂР°С‚СѓСЂС‹
//      board_temp_adc_filtered = board_temp_adc_filtered * 0.99 + (float)adc_buffer.temp * 0.01;
//      board_temp_deg_c = ((float)TEMP_CAL_HIGH_DEG_C - (float)TEMP_CAL_LOW_DEG_C) / ((float)TEMP_CAL_HIGH_ADC - (float)TEMP_CAL_LOW_ADC) * (board_temp_adc_filtered - (float)TEMP_CAL_LOW_ADC) + (float)TEMP_CAL_LOW_DEG_C;
//  	if (rosNow().sec > 1559347200){
//  		poweroff();
//  	}


  	if(w_to_l == 0 | w_to_r == 0){
  		last_pid_w_r = 0.0;
  		last_pid_w_l = 0.0;


  	}




		time_cmd_vel_now = HAL_GetTick();
		time_cmd_vel_d = time_cmd_vel_now - time_cmd_vel_last;

		usSRegHoldBuf[9] = time_cmd_vel_d;

		if (time_cmd_vel_d > 10000)
		{
//			usSRegHoldBuf[5] = 0;
//			usSRegHoldBuf[6] = 0;
			w_to_l = 0;
			w_to_r = 0;
//			speed_x = 0;
//			speed_w = 0;

			last_pid_w_r = 0.0;
			last_pid_w_l = 0.0;
		}





	speedL=speedL2;
	speedR=speedR2;




    #ifdef ADDITIONAL_CODE
      ADDITIONAL_CODE;
    #endif


    // ####### SET OUTPUTS #######
    if ((speedL < lastSpeedL + 50 && speedL > lastSpeedL - 50) && (speedR < lastSpeedR + 50 && speedR > lastSpeedR - 50) && timeout < TIMEOUT) {
    #ifdef INVERT_R_DIRECTION
//      pwmr = speedR;
    #else
      pwmr = -speedR;
    #endif
    #ifdef INVERT_L_DIRECTION
//      pwml = -speedL;
    #else
      pwml = speedL;
    #endif
    }

    lastSpeedL = speedL;
    lastSpeedR = speedR;


    // ####### POWEROFF BY POWER-BUTTON #######
//    if (HAL_GPIO_ReadPin(BUTTON_PORT, BUTTON_PIN) && weakr == 0 && weakl == 0) {
	if (HAL_GPIO_ReadPin(BUTTON_PORT, BUTTON_PIN) ) {
	      for(int i =0; i<=10; i++){
	      HAL_GPIO_TogglePin(LED_PORT, LED_PIN);
	      HAL_Delay(200);
	      }

		enable = 0;
      while (HAL_GPIO_ReadPin(BUTTON_PORT, BUTTON_PIN)) {}
      poweroff();


	}

    // ####### BEEP AND EMERGENCY POWEROFF #######
    if ((TEMP_POWEROFF_ENABLE && board_temp_deg_c >= TEMP_POWEROFF && abs(speed) < 20) || (batteryVoltage < ((float)BAT_LOW_DEAD * (float)BAT_NUMBER_OF_CELLS) && abs(speed) < 20)) {  // poweroff before mainboard burns OR low bat 3
      poweroff();
    } else if (TEMP_WARNING_ENABLE && board_temp_deg_c >= TEMP_WARNING) {  // beep if mainboard gets hot
      buzzerFreq = 4;
      buzzerPattern = 1;
    } else if (batteryVoltage < ((float)BAT_LOW_LVL1 * (float)BAT_NUMBER_OF_CELLS) && batteryVoltage > ((float)BAT_LOW_LVL2 * (float)BAT_NUMBER_OF_CELLS) && BAT_LOW_LVL1_ENABLE) {  // low bat 1: slow beep
      buzzerFreq = 5;
      buzzerPattern = 42;
    } else if (batteryVoltage < ((float)BAT_LOW_LVL2 * (float)BAT_NUMBER_OF_CELLS) && batteryVoltage > ((float)BAT_LOW_DEAD * (float)BAT_NUMBER_OF_CELLS) && BAT_LOW_LVL2_ENABLE) {  // low bat 2: fast beep
      buzzerFreq = 5;
      buzzerPattern = 6;
    } else if (BEEPS_BACKWARD && speed < -50) {  // backward beep
      buzzerFreq = 5;
      buzzerPattern = 1;
    } else {  // do not beep
//      buzzerFreq = 0;
//      buzzerPattern = 0;
    }


    // ####### INACTIVITY TIMEOUT #######
    if (abs(speedL) >=0 || abs(speedR) >= 0) {
      inactivity_timeout_counter = 0;
    } else {
      inactivity_timeout_counter ++;
    }
    if (inactivity_timeout_counter > (INACTIVITY_TIMEOUT * 60 * 1000) / (DELAY_IN_MAIN_LOOP + 1)) {  // rest of main loop needs maybe 1ms
      poweroff();
    }
  }
}

/** System Clock Configuration
*/
static void SystemClock_Config(void) {
  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_PeriphCLKInitTypeDef PeriphClkInit;

  /**Initializes the CPU, AHB and APB busses clocks
    */
  RCC_OscInitStruct.OscillatorType      = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState            = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = 16;
  RCC_OscInitStruct.PLL.PLLState        = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource       = RCC_PLLSOURCE_HSI_DIV2;
  RCC_OscInitStruct.PLL.PLLMUL          = RCC_PLL_MUL16;
  HAL_RCC_OscConfig(&RCC_OscInitStruct);

  /**Initializes the CPU, AHB and APB busses clocks
    */
  RCC_ClkInitStruct.ClockType      = RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource   = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider  = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2);

  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_ADC;
  PeriphClkInit.AdcClockSelection    = RCC_ADCPCLK2_DIV8;  // 8 MHz
  HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit);

  /**Configure the Systick interrupt time
    */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq() / 1000);

  /**Configure the Systick
    */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}


static void MX_USART2_UART_Init(void)
{

  /* USER CODE BEGIN USART2_Init 0 */

  /* USER CODE END USART2_Init 0 */

  /* USER CODE BEGIN USART2_Init 1 */

  /* USER CODE END USART2_Init 1 */
  huart2.Instance = USART2;
//  huart2.Init.BaudRate = 115200;
  huart2.Init.BaudRate = 57600;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
//    Error_Handler();
  }
  /* USER CODE BEGIN USART2_Init 2 */

  /* USER CODE END USART2_Init 2 */

}








void poweroff(void) {
    if (abs(speed) < 20) {
        buzzerPattern = 0;
        enable = 0;
        for (int i = 0; i < 8; i++) {
            // buzzerFreq = i;
            HAL_Delay(100);
        }
        HAL_GPIO_WritePin(OFF_PORT, OFF_PIN, GPIO_PIN_RESET);
        while(1) {}
    }
}



